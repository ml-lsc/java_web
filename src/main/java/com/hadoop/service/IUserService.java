package com.hadoop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hadoop.domain.User;

public interface IUserService extends IService<User> {
}
