# 数据可视化智能分析平台

## 技术架构

### 前端 : Vue+Element UI+ECharts

### 后端: SpringBoot

### 数据库:MySQL+Redis

### 细节技术: 

** echarts图表库导入**

** excel文件数据的解析和导入.**

** 动态多数据源的配置.**

** 第三方数据接口导入.**

## 应用环境

### JDK1.8  MySQL Redis Node.js