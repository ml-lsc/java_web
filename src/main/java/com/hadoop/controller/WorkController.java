package com.hadoop.controller;

import com.hadoop.domain.Apply;
import com.hadoop.domain.R;
import com.hadoop.filter.BaseResponse;
import com.hadoop.service.impl.WorkServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@BaseResponse
@RestController
@RequestMapping("/hadoop")
public class WorkController {

    @Autowired
    private WorkServiceImpl workService;

    @PostMapping("/saveApply")
    public R saveApply(@RequestBody Apply apply) {
        boolean b = workService.saveApply(apply);
        if (b){
            return new R(true,"保存成功!",null);
        }else {
            return new R(false,"保存失败!",null);
        }
    }

    @GetMapping("/getApplyList/{username}")
    public Object getApplyList(@PathVariable Integer username){
        List<Apply> applyList = workService.getApplyList(username);
        return applyList;
    }

    @DeleteMapping("/delApply/{username}/{id}")
    public R delApply(@PathVariable Integer username,@PathVariable String id){
        boolean b = workService.delApply(username, id);
        if (b){
            return new R(true,"删除成功!",null);
        }else {
            return new R(false,"删除失败!",null);
        }
    }


}
