package com.hadoop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hadoop.domain.Call;
import com.hadoop.domain.DataSource;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DataMapper extends BaseMapper<DataSource> {

    //查询库中所有表名
    @Select("select table_name from information_schema.tables where table_schema=#{name} ")
    List<String> getTableList(String name);

    //查询表中所有字段名
    @Select("select column_name from information_schema.columns where table_name=#{name} ")
    List<String> getFieldList(String name);

    //求和
    @Select("select sum(${s}) from compater.${table} ")
    Long getSum(String s, String table);

    @Select("select * from compater.${tableName}")
    List<Call> getTableData(String tableName);

    @Select("select * from compater.callTb where phone=#{phone}")
    List<Call> sql(String phone);


    @Select("select #{s} from compater.${table}")
    List<Double> getdata(String s, String table);
}
