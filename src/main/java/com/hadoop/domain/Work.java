package com.hadoop.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
@TableName("work")
@Data
public class Work {
    private String name;
    private Integer nomalDay;
    private Integer addDay;
    private Integer sleepDay;
    private Integer realDay;
}
