package com.hadoop.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "datasource_tb",autoResultMap = true)
public class DataSource {

    @TableId(type = IdType.AUTO)
    private Long id;
    private String linkname;
    private String hostname;
    private Long port;
    private String username;
    private String password;
    private String db;
    private Long uid;
}
