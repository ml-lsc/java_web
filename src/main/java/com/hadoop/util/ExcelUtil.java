package com.hadoop.util;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.builder.ExcelReaderBuilder;
import com.hadoop.listener.ExcelListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class ExcelUtil {
    /**
     *
     * @param file 用户上传的文件
     * @param sheet 选择读取的工作表 默认从 1 开始!
     * @param deleteHead 是否删除头! 默认选择删除!
     * @return
     * @throws IOException
     */
    public static Object read(File file, int sheet, boolean deleteHead) throws IOException {
        // 0. 获取文件输入流!
        InputStream inputStream = new FileInputStream(file);

        // 1. 创建监听器!
        ExcelListener listener = new ExcelListener();

        // 调用EasyExcel API 从流中读取数据!
        ExcelReaderBuilder reader = EasyExcel.read(inputStream, null, listener);

        // 默认选择第一个sheet进行读取! 当然也可以指定第几个sheet!
        reader.sheet(sheet).doRead();

        // 获取监听器解析的数据!
        List<Map<Integer, String>> data = listener.getData();

        // 获取表格的首行 即字段!!
        // 暂时不知道你需要那种类型! 所以就没做解析,而是把首航添加进去了!
        Map<Integer, String> head = listener.getHead();
        data.set(0,head);
        // 关闭流对象!
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data; // 返回解析结果!
    }
}
