package com.hadoop.service;

import com.hadoop.domain.Apply;

import java.util.List;

public interface IWorkService {

    boolean saveApply(Apply apply);
    List<Apply> getApplyList(Integer username);
    boolean delApply(Integer username, String id);
}
