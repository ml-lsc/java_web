package com.hadoop.controller;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.hadoop.domain.*;
import com.hadoop.filter.BaseResponse;
import com.hadoop.service.impl.DataSourceImpl;
import com.hadoop.service.impl.UploadServiceImpl;
import com.hadoop.util.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@BaseResponse
@RestController
@RequestMapping("/hadoop")
public class DataController {

    @Autowired
    private DataSourceImpl dataSourceImpl;

    @Autowired
    private UploadServiceImpl uploadService;

    public List<String> paths=new ArrayList<String>();

    //查询数据源
    @GetMapping("/datalist/{uid}")
    public List<DataSource> getData(@PathVariable Long uid){
        List<DataSource> dataSource = dataSourceImpl.getDataSource(uid);
        return dataSource;
    }

    //添加数据源
    @PostMapping("/datalink")
    public R addSource(@RequestBody com.hadoop.domain.DataSource ds){
        Set<String> strings = dataSourceImpl.addSource(ds);
        if (strings.size()>0){
            return new R(true,"连接成功!",null);
        }else {
            return new R(false,"连接失败!",null);
        }
    }

    //删除数据源
    @DeleteMapping("/remove/{id}")
    public R removeDataSource(@PathVariable Long id){
        boolean b = dataSourceImpl.removeDS(id);
        if (b){
            return new R(true,"删除成功!",null);
        }else {
            return new R(false,"删除失败!",null);
        }
    }

    //分页查询数据表
    @DS("compater")
    @GetMapping("/database/{id}/{currentPage}/{pageSize}")
    public PageTable getTable(@PathVariable Long id,@PathVariable Long currentPage,@PathVariable Long pageSize){
        return dataSourceImpl.getDataBase(id,currentPage,pageSize);
    }

    //查询全部数据表
    @DS("compater")
    @GetMapping("/databaseall/{id}")
    public PageTable getTableAll(@PathVariable Long id){
        return dataSourceImpl.getDataBase(id);
    }

    //查询表名
    @DS("compater")
    @GetMapping("/getFieldList")
    public List<String> getFieldList(String name){
        return dataSourceImpl.getFieldList(name);
    }

    @DS("compater")
    @PostMapping("/dataAnalysis")
    public List<AAA> dataAnalysis(@RequestBody Table table){
        List<AAA> dataA = dataSourceImpl.dataAnalysis(table);
        return dataA;
    }

    //文件解析
    @DS("exam")
    @PostMapping("/excelAnalysis")
    @ResponseBody
    public Object ExcelAnalysis(MultipartFile file) throws IOException {
        if(file == null) return "file is null!";
        String path = uploadService.saveFile(file);
        paths.add(path);
        Object read = ExcelUtil.read(new File(uploadService.get()+path), 0, false);
        return read;
    }

    //查询文件名称
    @GetMapping("/getFile")
    public List<FFF> getFile(){
        List<FFF> list = new ArrayList<>();
        for (String path : paths) {
            FFF fff = new FFF();
            fff.setName(path);
            list.add(fff);
        }
        return list;
    }

    @GetMapping("/getTableData")
    public List<Call> getCall(String tableName){
        return dataSourceImpl.getTableData(tableName);
    }

    @GetMapping("/sql")
    public List<Call> sql(String tableName){
        return dataSourceImpl.getTableData2(tableName);
    }

    @DS("compater")
    @PostMapping("/mathUtils")
    public List<AAA> mathUtils(@RequestBody Table table){
        return dataSourceImpl.mathUtils(table);
    }

}
