package com.hadoop.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
@TableName("salary")
@Data
public class Salary {
    private String name;
    private Integer baseSalary;
    private Integer otherSalary;
    private Integer sum;
}
