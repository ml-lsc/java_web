package com.hadoop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hadoop.domain.User;
import com.hadoop.mapper.UserMapper;
import com.hadoop.service.IUserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper,User> implements IUserService {
}
