package com.hadoop.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("user_tb")
public class User {
    private Long id;
    private String email;
    private Integer username;
    private String password;

}
