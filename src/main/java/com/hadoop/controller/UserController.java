package com.hadoop.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hadoop.domain.R;
import com.hadoop.domain.User;
import com.hadoop.filter.BaseResponse;
import com.hadoop.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@BaseResponse
@RestController
@RequestMapping("/hadoop")
public class UserController {

    @Autowired
    private UserServiceImpl userService;


    @PostMapping("/login")
    public R login(@RequestBody User user){
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username",user.getUsername())
               .eq("password",user.getPassword());
        if (userService.getOne(wrapper) == null){
            return new R(false,"用户名或密码错误!",null);
        }else {
            return new R(true,"登录成功!",userService.getOne(wrapper).getId());
        }
    }

    @GetMapping("/getUserInfo/{username}")
    public Object getUserInfo(@PathVariable Integer username){
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username",username);
        if (userService.getOne(wrapper) == null){
            return new R(false,"服务器出错了!",null);
        }else {
            User one = userService.getOne(wrapper);
            one.setPassword(null);
            return one;
        }
    }

    @PutMapping("/updateUserInfo")
    public R updateUserInfo(@RequestBody User user){
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username",user.getUsername());
        if (userService.update(user,wrapper)){
            return new R(true,"修改成功!",user);
        }else {
            return new R(false,"修改失败!",user);
        }
    }


}
