package com.hadoop.service.impl;

import com.baomidou.dynamic.datasource.DynamicRoutingDataSource;
import com.baomidou.dynamic.datasource.creator.DefaultDataSourceCreator;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hadoop.domain.*;
import com.hadoop.mapper.DataMapper;
import com.hadoop.service.IDataSource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class DataSourceImpl extends ServiceImpl<DataMapper, DataSource> implements IDataSource {


    @Resource
    private DataMapper dataMapper;

    @Resource
    private javax.sql.DataSource dataSource;
    @Resource
    private DefaultDataSourceCreator defaultDataSourceCreator;


    @Override
    public Set<String> addSource(DataSource ds) {
        dataMapper.insert(ds);
        DataAll da = new DataAll();
        da.setDriverClassName("com.mysql.cj.jdbc.Driver");
        da.setPoolName(ds.getLinkname());
        da.setUrl("jdbc:mysql://"+ds.getHostname()+":"+ds.getPort()+"/"+ds.getDb()+"?serverTimezone=UTC");
        da.setUsername(ds.getUsername());
        da.setPassword(ds.getPassword());
        DataSourceProperty dataSourceProperty = new DataSourceProperty();
        BeanUtils.copyProperties(da,dataSourceProperty);
        DynamicRoutingDataSource source = (DynamicRoutingDataSource) dataSource;
        javax.sql.DataSource dataSource = defaultDataSourceCreator.createDataSource(dataSourceProperty);
        source.addDataSource(da.getPoolName(),dataSource);
        return source.getDataSources().keySet();
    }

    @Override
    public List<DataSource> getDataSource(Long uid) {
        QueryWrapper<DataSource> wrapper = new QueryWrapper<>();
        wrapper.eq("uid",uid);
        return dataMapper.selectList(wrapper);
    }

    @Override
    public boolean removeDS(Long id) {
        QueryWrapper<DataSource> wrapper = new QueryWrapper<>();
        wrapper.eq("id",id);
        DataSource one = dataMapper.selectOne(wrapper);
        DynamicRoutingDataSource source = (DynamicRoutingDataSource) this.dataSource;
        source.removeDataSource(one.getLinkname());
        return dataMapper.deleteById(id)>0;
    }

    @Override
    public PageTable getDataBase(Long id,Long currentPage,Long pageSize) {
        String db="compater";
        List<String> exam = dataMapper.getTableList(db);
        List<DataBase> list = new ArrayList<>();
        for (String s : exam) {
            DataBase dataBase = new DataBase();
            dataBase.setDb(db);
            dataBase.setTable(s);
            list.add(dataBase);
        }
        PageTable table = new PageTable();
        long start=(currentPage-1)*pageSize;
        long end=currentPage*pageSize;
        if (end>list.size()){
            end=list.size();
        }
        table.setList(list.subList((int) start, (int) end));
        table.setPageTotal((long) list.size());
        table.setCurrentPage(currentPage);
        return table;
    }

    @Override
    public PageTable getDataBase(Long id) {
        String db="compater";
        List<String> exam = dataMapper.getTableList(db);
        List<DataBase> list = new ArrayList<>();
        for (String s : exam) {
            DataBase dataBase = new DataBase();
            dataBase.setDb(db);
            dataBase.setTable(s);
            list.add(dataBase);
        }
        PageTable table = new PageTable();
        table.setList(list);
        return table;
    }

    @Override
    public List<String> getFieldList(String name) {
        return dataMapper.getFieldList(name);
    }

    //数据分析
    @Override
    public List<AAA> dataAnalysis(Table table) {
        List<AAA> list = new ArrayList<>();
        if (table.getFields().isEmpty()){
            return null;
        }else {
            for (String s : table.getFields()) {
                Long sum = dataMapper.getSum(s, table.getTable());
                AAA aaa = new AAA();
                aaa.setName(s);
                aaa.setValue(sum);
                list.add(aaa);
            }
        }
        return list;
    }

    @Override
    public List<Call> getTableData(String tableName) {
       return dataMapper.getTableData(tableName);
    }



    //获取数据源集合
    public Set<String> getData(){
        DynamicRoutingDataSource source = (DynamicRoutingDataSource) dataSource;
        return source.getDataSources().keySet();
    }

    public List<Call> getTableData2(String tableName) {
        return dataMapper.sql(tableName);
    }

    //计算方差
    @Override
    public List<AAA> mathUtils(Table table) {
        List<AAA> list = new ArrayList<>();
        if (table.getFields().isEmpty()){
            return null;
        }else {
            for (String s : table.getFields()) {
                Long sum = dataMapper.getSum(s, table.getTable());
//                double variance = MathUtil.Variance(sum);
                AAA aaa = new AAA();
                aaa.setName(s);
                aaa.setValue(sum/2);
                list.add(aaa);
            }
        }
        return list;
    }
}
