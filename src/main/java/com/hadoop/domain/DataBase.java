package com.hadoop.domain;

import lombok.Data;

@Data
public class DataBase {

    private String db;
    private String table;

}
