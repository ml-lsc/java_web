package com.hadoop.domain;

import lombok.Data;

@Data
public class DataAll {
    private String poolName;
    private String driverClassName;
    private String url;
    private String username;
    private String password;
}
