package com.hadoop.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
@TableName("score")
@Data
public class Score {
    private Integer exam;
    private Integer selectScore;
    private Integer blobScore;
    private Integer delatiScore;
    private Integer sumScore;

}
