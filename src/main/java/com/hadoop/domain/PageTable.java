package com.hadoop.domain;

import lombok.Data;

import java.util.List;
@Data
public class PageTable {
    private List<DataBase> list;
    private Long pageTotal;
    private Long pageSize;
    private Long currentPage;
}
