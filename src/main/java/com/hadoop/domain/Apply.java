package com.hadoop.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.lang.reflect.Array;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Apply implements Serializable {
    private String id;
    private String createTime;
    private String theme;
    private String background;
    private Array chartList[];
    private String screenShot;
    private String name;
    private Integer username;

}
