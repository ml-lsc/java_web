package com.hadoop.domain;

import lombok.Data;

import java.util.List;

@Data
public class Table {
    private List<String> fields;
    private String table;
    private Integer type;
}
