package com.hadoop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hadoop.domain.*;

import java.util.List;
import java.util.Set;

public interface IDataSource extends IService<DataSource> {
    Set<String> addSource(DataSource ds);

    List<DataSource> getDataSource(Long uid);

    boolean removeDS(Long id);

    PageTable getDataBase(Long id, Long currentPage, Long pageSize);

    PageTable getDataBase(Long id);

    List<String> getFieldList(String name);

    List<AAA> dataAnalysis(Table table);

    List<Call> getTableData(String tableName);

    List<Call> getTableData2(String tableName);

    List<AAA> mathUtils(Table table);
}
