package com.hadoop.domain;

import lombok.Data;

import java.util.List;
@Data
public class DataA {
    private List<String> field;
    private List<Long> sum;
}
