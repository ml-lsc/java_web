package com.hadoop.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExcelListener extends AnalysisEventListener<Map<Integer,String>> {
    /**
     *  存储解析获得的数据!
     */
    private List<Map<Integer,String>> data = new ArrayList<>();
    private Map<Integer,String> head;


    @Override
    public void invoke(Map<Integer, String> integerStringMap, AnalysisContext analysisContext) {
        data.add(integerStringMap);
        System.out.println(integerStringMap);
    }

    @Override
    public void invokeHeadMap(Map headMap, AnalysisContext context) {
        head = headMap; // 获取首行!
        System.out.println(headMap);
    }



    /**
     *  获取到每一行 需要做的业务逻辑!
     * @param analysisContext
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }

    public List<Map<Integer, String>> getData() {

        return data;
    }

    public void setData(List<Map<Integer, String>> data) {
        this.data = data;
    }

    public Map<Integer, String> getHead() {
        return head;
    }

    public void setHead(Map<Integer, String> head) {
        this.head = head;
    }
}
