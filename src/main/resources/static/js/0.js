(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./src/components/chartConfig/form/index.js":
/*!**************************************************!*\
  !*** ./src/components/chartConfig/form/index.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ \"./node_modules/core-js/modules/es.object.to-string.js\");\n/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__);\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  title: function title() {\n    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./title.vue */ \"./src/components/chartConfig/form/title.vue\"));\n  },\n  legend: function legend() {\n    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./legend.vue */ \"./src/components/chartConfig/form/legend.vue\"));\n  },\n  text: function text() {\n    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./text.vue */ \"./src/components/chartConfig/form/text.vue\"));\n  },\n  setting: function setting() {\n    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./setting.vue */ \"./src/components/chartConfig/form/setting.vue\"));\n  }\n});\n\n//# sourceURL=webpack:///./src/components/chartConfig/form/index.js?");

/***/ })

}]);