package com.hadoop.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("callTb")
@Data
public class Call {
    private String phone;
    private Integer sentTimes;
    private Integer recTimes;
}
